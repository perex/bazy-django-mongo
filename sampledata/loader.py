#!/bin/python

import xml.etree.ElementTree as ET
import os
import yaml

northwind_database = {}

files = os.listdir(".")
#print files

for fil in files:
	if fil.endswith(".xml"):
		print "Processing file " + fil + "..."
		tree = ET.parse(fil)
		root = tree.getroot()
		if root.tag not in northwind_database:
			northwind_database[root.tag] = []
		for child in root:
			row = {}
			for olderchild in child:
				row[olderchild.tag] = olderchild.text
			northwind_database[root.tag].append(row)

#for detail in northwind_database["Orders"]:
#	print detail["OrderID"]

#os.exit(0)

print "Preparing data for being send to db..."

for product in northwind_database["Products"]:
	for supplier in northwind_database["Suppliers"]:
		if product["SupplierID"]==supplier["SupplierID"]:
			product["SupplierName"] = supplier["CompanyName"]	

print "Products done"

for detail in northwind_database["OrderDetails"]:
	for product in northwind_database["Products"]:
		if product["ProductID"] == detail["ProductID"]:
			detail["ProductPrice"] = product["UnitPrice"]

print "OrderDetails done"

northwind_database["OrdersModified"] = {}
for order in northwind_database["Orders"]:
	order["OrderDetailsList"] = []
	northwind_database["OrdersModified"][order["OrderID"]] = order

del northwind_database["Orders"]
	
for detail in northwind_database["OrderDetails"]:
	northwind_database["OrdersModified"][detail["OrderID"]]["OrderDetailsList"].append(detail)


#for order in northwind_database["Orders"]:
#	order["OrderDetails"] = []
#	for detail in northwind_database["OrderDetails"]:
#		#print order["OrderID"], detail["OrderID"]
#		if order["OrderID"] == detail["OrderID"]:
#			#print "Napierdalam!!!"
#			order["OrderDetails"].append(detail)
			

print "Orders done"

northwind_database["Orders"] = []

for order in northwind_database["OrdersModified"]:
	northwind_database["Orders"].append(northwind_database["OrdersModified"][order])

del northwind_database["OrdersModified"]

stream = file('northwind.yaml', 'w')
yaml.dump(northwind_database, stream)

print "Yaml created. Finished"
