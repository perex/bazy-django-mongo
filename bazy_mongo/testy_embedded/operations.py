#!/usr/bin/env python
from models import *
import datetime
class Operations:
    def __init__(self):
        self.categories=[]
        self.suppliers=[]
        self.products=[]
        self.employees=[]
        self.shippers=[]
        self.customers=[]
        self.order_details=[]
        self.orders=[]
        self.orders_manager=Orders.objects

    def insert_into(self, collection, data):
        pass

#Categories
    def insert_into_Categories(self, CategoryId, CategoryName, Description):
        category = Categories(CategoryId=CategoryId, CategoryName = CategoryName, Description = Description)
        self.categories.append(category)
    def save_Categories(self):
        for c in self.categories:
            c.save()

#Suppliers
    def insert_into_Suppliers(self, SupplierID, CompanyName, ContactName, ContactTitle, Address, PostalCode, City, Country, Phone):
        supplier = Suppliers(SupplierID=SupplierID, CompanyName=CompanyName, ContactName=ContactName, ContactTitle=ContactTitle, Address=Address, PostalCode=PostalCode, City=City, Country=Country, Phone=Phone)
        self.suppliers.append(supplier)
    def save_Suppliers(self):
        for s in self.suppliers:
            s.save()

#Products
    def insert_into_Products(self, ProductID, ProductName, SupplierID, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, UnitsOnOrder, ReorderLevel, Discontinued, CompanyName):
        product = Products(ProductID=ProductID, ProductName=ProductName, SupplierID=SupplierID, CategoryID=CategoryID, QuantityPerUnit=QuantityPerUnit, UnitPrice=UnitPrice, UnitsInStock=UnitsInStock, UnitsOnOrder=UnitsOnOrder, ReorderLevel=ReorderLevel, Discontinued=Discontinued, CompanyName=CompanyName)
        self.products.append(product)
    def save_Products(self):
        for p in self.products:
            p.save()

#Employees
    def insert_into_Employees(self, EmployeeId, LastName, FirstName, Title, TitleOfCourtesy, BirthDate, HireDate, Address, City, Region, PostalCode, Country, HomePhone, Extension, Notes, ReportsTo):
        employee = Employees(EmployeeId=EmployeeId, LastName=LastName, FirstName=FirstName, Title=Title, TitleOfCourtesy=TitleOfCourtesy, BirthDate=BirthDate, HireDate=HireDate, Address=Address, City=City, Region=Region, PostalCode=PostalCode, Country=Country, HomePhone=HomePhone, Extension=Extension, Notes=Notes, ReportsTo=ReportsTo)
        self.employees.append(employee)
    def save_Employees(self):
        for e in self.employees:
            e.save()

#Shippers
    def insert_into_Shippers(self, ShipperID, CompanyName, Phone):
        shipper = Shippers(ShipperID=ShipperID, CompanyName=CompanyName, Phone=Phone)
        self.shippers.append(shipper)
    def save_Shippers(self):
        for s in self.shippers:
            s.save()

#Customers
    def insert_into_Customers(self, CustomerID, CompanyName, ContactName, ContactTitle, Address, City, PostalCode, Country, Phone, Fax):
        customer = Customers(CustomerID=CustomerID, CompanyName=CompanyName, ContactName=ContactName, ContactTitle=ContactTitle, Address=Address, City=City, PostalCode=PostalCode, Country=Country, Phone=Phone, Fax=Fax)
        self.customers.append(customer)
    def save_Customers(self):
        for c in self.customers:
            c.save()

#OrderDetails
    def insert_into_OrderDetails(self, ProductID, UnitPrice, Quantity, Discount, ProductPrice, SupplierName):
        order_detail = OrderDetails(ProductID=ProductID, UnitPrice=UnitPrice, Quantity=Quantity, Discount=Discount, ProductPrice=ProductPrice, SupplierName=SupplierName)
        self.order_details.append(order_detail)
    def save_OrderDetails(self):
        for o in self.order_details:
            o.save()
#Orders
    def insert_into_Orders(self, OrderID, CustomerID, EmployeeID, OrderDate, RequiredDate, ShippedDate, ShipVia, Freight, ShipName, ShipAddress, ShipCity, ShipPostalCode, ShipCountry, OrderDetailsList, CustomerCountry):
        if OrderDate=='0000-00-00':
            OrderDate=None
        if RequiredDate=='0000-00-00':
            RequiredDate=None
        if ShippedDate=='0000-00-00':
            ShippedDate=None
        order = Orders(OrderID=OrderID, CustomerID=CustomerID, EmployeeID=EmployeeID, OrderDate=OrderDate, RequiredDate=RequiredDate, ShippedDate=ShippedDate, ShipVia=ShipVia, Freight=Freight, ShipName=ShipName, ShipAddress=ShipAddress, ShipCity=ShipCity, ShipPostalCode=ShipPostalCode, ShipCountry=ShipCountry, OrderDetailsList=OrderDetailsList, CustomerCountry=CustomerCountry)
        self.orders.append(order)
    def save_Orders(self):
        for o in self.orders:
            o.save()

### GET ###

#Ile zamowien z kazdego kraju zostalo zrealizowanych
    def get_orders_from_countries(self):
	from django.db.models import Count
	
	res = {}
	countries = set()
	for country in Customers.objects.values("Country"):	
		countries.add(country['Country'])

	for country in countries:
		res[country] = Orders.objects.filter(CustomerCountry__exact=country).exclude(ShippedDate__exact=None).count()
	return res
	

#Jaki byl sredni czas realizacji zamowienia w kazdym roku
    def get_avg_order_time(self):
        from django.db.models import Avg
	res = {}
	
	for order in Orders.objects.all():
		year = order.OrderDate.year
		if not year in res:
			res[year] = {}
			res[year]['quantity'] = 0
			res[year]['value'] = datetime.timedelta()
		res[year]['quantity'] += 1
		res[year]['value'] += order.ShippedDate - order.OrderDate

	for year in res:
		res[year] = res[year]['value']/res[year]['quantity']
		res[year] = res[year].days + res[year].seconds/float(86400)
	return res

    def get_aaa(self):
	res = {}
	details_list = Orders.objects.all().values('OrderDetailsList')
	print details_list
	for detail in details_list:
		for product in detail['OrderDetailsList']:
			if not product['SupplierName'] in res:
				res[product['SupplierName']] = 0
			res[product['SupplierName']] += product['Quantity']
			

	return res

    def get_orders_sum_per_weekday(self):
        res = {}
        for order in Orders.objects.all():
            weekday = order.OrderDate.weekday()
	    if not weekday in res:
                res[weekday] = 0
            for detail in order.OrderDetailsList:
		res[weekday] += detail.UnitPrice*detail.Quantity*(1-detail.Discount)

	return res


    def get_ccc(self):
	res = {}
	for order in Orders.objects.all():
		year = order.OrderDate.year
		country = order.CustomerCountry
		if not country in res:
			res[country] = {}
	 	if not year in res[country]:
			res[country][year] = 0
		for detail in order.OrderDetailsList:
			res[country][year] += detail.UnitPrice*detail.Quantity

	return res

    def get_ddd(self):
	res = {}
	for order in Orders.objects.all():
		year = order.OrderDate.year
		shipper = order.ShipVia
		for detail in order.OrderDetailsList:
			if not shipper in res:
				res[shipper]={}
			if not year in res[shipper]:
				res[shipper][year] = {}
				res[shipper][year]['quantity'] = 0
				res[shipper][year]['value'] = 0
			res[shipper][year]['quantity']+=detail.Quantity
			res[shipper][year]['value']+=detail.UnitPrice*detail.Quantity

	for shipper in res:
		for year in res[shipper]:
			res[shipper][year]['avg'] = float(res[shipper][year]['value'])/float(res[shipper][year]['quantity'])	
			del res[shipper][year]['value']
			del res[shipper][year]['quantity']

	return res
   
