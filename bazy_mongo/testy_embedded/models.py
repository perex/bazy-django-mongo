from django.db import models
from django.core.urlresolvers import reverse
from django_mongodb_engine.contrib import MongoDBManager
from djangotoolbox.fields import ListField, EmbeddedModelField


class Categories(models.Model):
    CategoryId = models.TextField()
    CategoryName = models.TextField()
    Description = models.TextField()

class Suppliers(models.Model):
    SupplierID = models.IntegerField()
    CompanyName = models.TextField()
    ContactName = models.TextField()
    ContactTitle = models.TextField()
    Address = models.TextField()
    PostalCode = models.TextField()
    City = models.TextField()
    #Region
    Country = models.TextField()
    Phone = models.TextField()
    #Fax
    #HomePage

class Products(models.Model):
    ProductID = models.IntegerField()
    ProductName = models.TextField()
    SupplierID = models.IntegerField()
    CategoryID = models.IntegerField()
    QuantityPerUnit = models.TextField()
    UnitPrice = models.FloatField()
    UnitsInStock = models.IntegerField()
    UnitsOnOrder = models.IntegerField()
    ReorderLevel = models.IntegerField()
    Discontinued = models.BooleanField() ##IntegerField?
    #added: (Supplier)
    CompanyName = models.TextField()
    
class Employees(models.Model):
    EmployeeId = models.IntegerField()
    LastName = models.TextField()
    FirstName = models.TextField()
    Title = models.TextField()
    TitleOfCourtesy = models.TextField()
    BirthDate = models.DateField()
    HireDate = models.DateField()
    Address = models.TextField()
    City = models.TextField()
    Region = models.TextField()
    PostalCode = models.TextField()
    Country = models.TextField()
    HomePhone = models.TextField()
    Extension = models.IntegerField()
    #Photo
    Notes = models.TextField()
    ReportsTo = models.IntegerField()
    #PhotoPath

class Shippers(models.Model):
    ShipperID = models.IntegerField()
    CompanyName = models.TextField()
    Phone = models.TextField()

class Customers(models.Model):
    CustomerID = models.TextField()
    CompanyName = models.TextField()
    ContactName = models.TextField()
    ContactTitle = models.TextField()
    Address = models.TextField()
    City = models.TextField()
    #Region
    PostalCode = models.TextField()
    Country = models.TextField()
    Phone = models.TextField()
    Fax = models.TextField()

class OrderDetails(models.Model):
    #OrderID = models.TextField()
    ProductID = models.IntegerField()
    UnitPrice = models.FloatField()
    Quantity = models.IntegerField()
    Discount = models.FloatField()
    #added
    ProductPrice = models.IntegerField()
    SupplierName = models.TextField()

class Orders(models.Model):
    
    OrderID = models.IntegerField()
    CustomerID = models.TextField()
    CustomerCountry = models.TextField()
    EmployeeID = models.IntegerField()
    OrderDate = models.DateField()
    RequiredDate = models.DateField()
    ShippedDate = models.DateField(null=True)
    ShipVia = models.IntegerField()
    Freight = models.IntegerField()
    ShipName = models.TextField()
    ShipAddress = models.TextField()
    ShipCity = models.TextField()
    #ShipRegion
    ShipPostalCode = models.TextField()
    ShipCountry = models.TextField()
    #added
    OrderDetailsList = ListField(EmbeddedModelField('OrderDetails'))

#For test:

