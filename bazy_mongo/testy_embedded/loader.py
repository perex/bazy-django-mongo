#!/bin/python

import xml.etree.ElementTree as ET
import os
from operations import Operations
import pickle
from models import *
import time
import datetime

def make_database():
	fil = file('testy_embedded/data/northwind.pickle')	
	northwind_database = pickle.load(fil)
	return northwind_database

def insert_categories(categories_dict):
	op = Operations()
	for entry in categories_dict:
		op.insert_into_Categories(entry["CategoryID"], entry["CategoryName"], entry["Description"])
		op.save_Categories()

def insert_suppliers(suppliers_dict):
	op = Operations()
	for entry in suppliers_dict:
		op.insert_into_Suppliers(entry['SupplierID'], entry['CompanyName'], entry['ContactName'], entry['ContactTitle'], entry['Address'], entry['PostalCode'], entry['City'], entry['Country'], entry['Phone'])
		op.save_Suppliers()	

def insert_products(products_dict):
	op = Operations()
	for entry in products_dict:
		op.insert_into_Products(entry['ProductID'], entry['ProductName'], int(entry['SupplierID']), entry['CategoryID'], entry['QuantityPerUnit'], entry['UnitPrice'], entry['UnitsInStock'], entry['UnitsOnOrder'], entry['ReorderLevel'], entry['Discontinued'], entry['SupplierName'])
		op.save_Products()

def insert_employees(employees_dict):
	op = Operations()
	for entry in employees_dict:
		try:
			op.insert_into_Employees(entry['EmployeeID'], entry['LastName'], entry['FirstName'], entry['Title'], entry['TitleOfCourtesy'], entry['BirthDate'], entry['HireDate'], entry['Address'], entry['City'], entry['Region'], entry['PostalCode'], entry['Country'], entry['HomePhone'], entry['Extension'], entry['Notes'], int(entry['ReportsTo']))
		except:
			op.insert_into_Employees(entry['EmployeeID'], entry['LastName'], entry['FirstName'], entry['Title'], entry['TitleOfCourtesy'], entry['BirthDate'], entry['HireDate'], entry['Address'], entry['City'], 'NULL', entry['PostalCode'], entry['Country'], entry['HomePhone'], entry['Extension'], entry['Notes'], int(entry['ReportsTo']))	
		op.save_Employees()

def insert_shippers(shippers_dict):
	op = Operations()
	for entry in shippers_dict:
		op.insert_into_Shippers(entry['ShipperID'], entry['CompanyName'], entry['Phone'])
		op.save_Shippers()

def insert_customers(customers_dict):
	op = Operations()
	for entry in customers_dict:
		try:
			op.insert_into_Customers(entry['CustomerID'], entry['CompanyName'], entry['ContactName'], entry['ContactTitle'], entry['Address'], entry['City'], entry['PostalCode'], entry['Country'], entry['Phone'], entry['Fax'])
		
		except:
			op.insert_into_Customers(entry['CustomerID'], entry['CompanyName'], entry['ContactName'], entry['ContactTitle'], entry['Address'], entry['City'], 'NULL', entry['Country'], entry['Phone'], 'NULL')

		op.save_Customers()

def insert_orders(orders_dict):
	f = open("loading_" + datetime.datetime.today().isoformat(), 'w')
	op = Operations()
	counter = 0
	timestamp = time.time()
	begin = timestamp
	for entry in orders_dict:
		#print entry['OrderID']
		order_details_list = []
		for or_det in entry["OrderDetailsList"]:
			order_details_list.append(OrderDetails(ProductID=int(or_det['ProductID']), UnitPrice=float(or_det['UnitPrice']), Quantity=int(or_det['Quantity']), Discount=float(or_det['Discount']), ProductPrice=int(or_det['ProductPrice']), SupplierName=or_det['SupplierName']))
			
		try:
			op.insert_into_Orders(int(entry['OrderID']), entry['CustomerID'], entry['EmployeeID'], entry['OrderDate'], entry['RequiredDate'], entry['ShippedDate'], entry['ShipVia'], entry['Freight'], entry['ShipName'], entry['ShipAddress'], entry['ShipCity'], entry['ShipPostalCode'], entry['ShipCountry'], order_details_list, entry['CustomerCountry'])
		except:
			op.insert_into_Orders(entry['OrderID'], entry['CustomerID'], entry['EmployeeID'], entry['OrderDate'], entry['RequiredDate'], entry['ShippedDate'], entry['ShipVia'], entry['Freight'], entry['ShipName'], entry['ShipAddress'], entry['ShipCity'], 'NULL', entry['ShipCountry'], order_details_list, entry['CustomerCountry'])	
		if counter!=0 and counter % 1000 == 0:
			op.save_Orders()
			cur_time = time.time()
			delta = cur_time - timestamp
			print delta
			f.write(str(delta))
			f.write("\n")
			timestamp = cur_time	
		counter = counter + 1

	
	if counter % 1000 != 0:
		op.save_Orders()
		cur_time = time.time()
		delta = cur_time - timestamp
		print delta
		f.write(str(delta))
		f.write("\n")
	cur_time = time.time()
	delta = cur_time - begin
	f.write("Whole time: ")
	f.write(str(delta))
	f.write("\n")
	f.close() 
