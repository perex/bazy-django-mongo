#!/usr/bin/env python

import datetime
import time
import testy_relacje.operations
import testy_embedded.operations
import testy_embedded_raw.operations

def test_fun(funkcja, nazwa, f):

    print("START: ", nazwa)
    f.write(nazwa)
    f.write("\n")
    start = time.time()
    ret = funkcja()
    stop = time.time()
    delta = stop - start
    print("STOP: ", nazwa)
    print("Time: ", delta)
    print(ret)

    f.write("Time:")
    f.write(str(delta))
    f.write("\n")
    f.write(str(ret))
    f.write("\n")

#test all
def test():
    oper = []
#    oper.append( testy_embedded.operations.Operations() )
#    oper.append( testy_embedded_raw.operations.Operations() )
    oper.append( testy_relacje.operations.Operations() )

    for op in oper:
        f = open("get_tests_" + str(op) + datetime.datetime.today().isoformat(), 'w')    
        test_fun(op.get_orders_from_countries, "get_orders_from_countries", f)
        test_fun(op.get_avg_order_time, "get_avg_order_time", f)
        test_fun(op.get_aaa, "get_aaa", f)
        test_fun(op.get_orders_sum_per_weekday, "get_orders_sum_per_weekday", f)
        test_fun(op.get_ccc, "get_ccc", f)
        test_fun(op.get_ddd, "get_ddd", f)
        f.close()
