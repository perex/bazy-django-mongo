from django.db import models
from django.core.urlresolvers import reverse

from djangotoolbox.fields import ListField, EmbeddedModelField

class Categories(models.Model):
    CategoryId = models.TextField(primary_key=True)
    CategoryName = models.TextField()
    Description = models.TextField()
    #Picture
    class Meta:
        db_table = 'north_categories'

class Suppliers(models.Model):
    SupplierID = models.IntegerField(primary_key=True)
    CompanyName = models.TextField()
    ContactName = models.TextField()
    ContactTitle = models.TextField()
    Address = models.TextField()
    PostalCode = models.TextField()
    City = models.TextField()
    #Region
    Country = models.TextField()
    Phone = models.TextField()
    #Fax
    #HomePage
    class Meta:
        db_table = 'north_suppliers'

class Products(models.Model):
    ProductID = models.IntegerField(primary_key=True)
    ProductName = models.TextField()
    SupplierID = models.ForeignKey(Suppliers)
    CategoryID = models.ForeignKey(Categories)
    QuantityPerUnit = models.TextField()
    UnitPrice = models.FloatField()
    UnitsInStock = models.IntegerField()
    UnitsOnOrder = models.IntegerField()
    ReorderLevel = models.IntegerField()
    Discontinued = models.BooleanField() ##IntegerField?
    #added: (Supplier)
    CompanyName = models.TextField()
    class Meta:
        db_table = 'north_products'

class Employees(models.Model):
    EmployeeId = models.IntegerField(primary_key=True)
    LastName = models.TextField()
    FirstName = models.TextField()
    Title = models.TextField()
    TitleOfCourtesy = models.TextField()
    BirthDate = models.DateField()
    HireDate = models.DateField()
    Address = models.TextField()
    City = models.TextField()
    Region = models.TextField()
    PostalCode = models.TextField()
    Country = models.TextField()
    HomePhone = models.TextField()
    Extension = models.IntegerField()
    #Photo
    Notes = models.TextField()
    ReportsTo = models.IntegerField() # models.ForeignKey('self') #Employees, self relationship
    #PhotoPath
    class Meta:
        db_table = 'north_employees'

class Shippers(models.Model):
    ShipperID = models.IntegerField(primary_key=True)
    CompanyName = models.TextField()
    Phone = models.TextField()
    class Meta:
        db_table = 'north_shippers'

class Customers(models.Model):
    CustomerID = models.TextField(primary_key=True)
    CompanyName = models.TextField()
    ContactName = models.TextField()
    ContactTitle = models.TextField()
    Address = models.TextField()
    City = models.TextField()
    #Region
    PostalCode = models.TextField()
    Country = models.TextField()
    Phone = models.TextField()
    Fax = models.TextField()
    class Meta:
        db_table = 'north_customers'

class Orders(models.Model):
    OrderID = models.IntegerField(primary_key=True)
    CustomerID = models.ForeignKey(Customers)
    EmployeeID = models.ForeignKey(Employees)
    OrderDate = models.DateField()
    RequiredDate = models.DateField()
    ShippedDate = models.DateField(null=True)
    ShipVia = models.ForeignKey(Shippers)
    Freight = models.IntegerField()
    ShipName = models.TextField()
    ShipAddress = models.TextField()
    ShipCity = models.TextField()
    #ShipRegion
    ShipPostalCode = models.TextField()
    ShipCountry = models.TextField()
    #added
#    OrderDetailsList = ListField(EmbeddedModelField('OrderDetails'))
    class Meta:
        db_table = 'north_orders'

class OrderDetails(models.Model):
    OrderID = models.ForeignKey(Orders)
    #depends on version
    ProductID = models.ForeignKey(Products)
    #ProductID = models.ForeignKey(Products)
    UnitPrice = models.FloatField()
    Quantity = models.IntegerField()
    Discount = models.FloatField()
    class Meta:
        db_table = 'north_order_details'
