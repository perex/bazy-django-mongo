#!/usr/bin/env python
from models import *

class Operations:
    def __init__(self):
        self.categories=[]
        self.suppliers=[]
        self.products=[]
        self.employees=[]
        self.shippers=[]
        self.customers=[]
        self.order_details=[]
        self.orders=[]
        self.orders_manager = Orders.objects
        self.products_manager = Products.objects
        self.employees_manager = Employees.objects
        self.customers_manager = Customers.objects
        self.shippers_manager = Shippers.objects
        self.order_details_manager = OrderDetails.objects
    def insert_into(self, collection, data):
        pass

#Categories
    def insert_into_Categories(self, CategoryId, CategoryName, Description):
        category = Categories(CategoryId=CategoryId, CategoryName = CategoryName, Description = Description)
        self.categories.append(category)
    def save_Categories(self):
        for c in self.categories:
            c.save()

#Suppliers
    def insert_into_Suppliers(self, SupplierID, CompanyName, ContactName, ContactTitle, Address, PostalCode, City, Country, Phone):
        supplier = Suppliers(SupplierID=SupplierID, CompanyName=CompanyName, ContactName=ContactName, ContactTitle=ContactTitle, Address=Address, PostalCode=PostalCode, City=City, Country=Country, Phone=Phone)
        self.suppliers.append(supplier)
    def save_Suppliers(self):
        for s in self.suppliers:
            s.save()

#Products
    def insert_into_Products(self, ProductID, ProductName, SupplierID, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, UnitsOnOrder, ReorderLevel, Discontinued, CompanyName):
        supplier = Suppliers.objects.get(pk=SupplierID)
        category = Categories.objects.get(pk=CategoryID)
        product = Products(ProductID=ProductID, ProductName=ProductName, SupplierID=supplier, CategoryID=category, QuantityPerUnit=QuantityPerUnit, UnitPrice=UnitPrice, UnitsInStock=UnitsInStock, UnitsOnOrder=UnitsOnOrder, ReorderLevel=ReorderLevel, Discontinued=Discontinued, CompanyName=CompanyName)
        self.products.append(product)
    def save_Products(self):
        for p in self.products:
            p.save()

#Employees
    def insert_into_Employees(self, EmployeeId, LastName, FirstName, Title, TitleOfCourtesy, BirthDate, HireDate, Address, City, Region, PostalCode, Country, HomePhone, Extension, Notes, ReportsTo):
        reports_to_employee = ReportsTo # Employees.objects.get(pk=ReportsTo)
        employee = Employees(EmployeeId=EmployeeId, LastName=LastName, FirstName=FirstName, Title=Title, TitleOfCourtesy=TitleOfCourtesy, BirthDate=BirthDate, HireDate=HireDate, Address=Address, City=City, Region=Region, PostalCode=PostalCode, Country=Country, HomePhone=HomePhone, Extension=Extension, Notes=Notes, ReportsTo=reports_to_employee)
        self.employees.append(employee)
    def save_Employees(self):
        for e in self.employees:
            e.save()

#Shippers
    def insert_into_Shippers(self, ShipperID, CompanyName, Phone):
        shipper = Shippers(ShipperID=ShipperID, CompanyName=CompanyName, Phone=Phone)
        self.shippers.append(shipper)
    def save_Shippers(self):
        for s in self.shippers:
            s.save()

#Customers
    def insert_into_Customers(self, CustomerID, CompanyName, ContactName, ContactTitle, Address, City, PostalCode, Country, Phone, Fax):
        customer = Customers(CustomerID=CustomerID, CompanyName=CompanyName, ContactName=ContactName, ContactTitle=ContactTitle, Address=Address, City=City, PostalCode=PostalCode, Country=Country, Phone=Phone, Fax=Fax)
        self.customers.append(customer)
    def save_Customers(self):
        for c in self.customers:
            c.save()

#OrderDetails
    def insert_into_OrderDetails(self, OrderID, ProductID, UnitPrice, Quantity, Discount):
        order = self.orders_manager.get(pk=OrderID)
        product = self.products_manager.get(pk=ProductID)
        order_detail = OrderDetails(OrderID = order, ProductID=product, UnitPrice=UnitPrice, Quantity=Quantity, Discount=Discount)
        self.order_details.append(order_detail)
    def save_OrderDetails(self):
        for o in self.order_details:
            o.save()
#Orders
    def insert_into_Orders(self, OrderID, CustomerID, EmployeeID, OrderDate, RequiredDate, ShippedDate, ShipVia, Freight, ShipName, ShipAddress, ShipCity, ShipPostalCode, ShipCountry):
        if OrderDate=='0000-00-00':
            OrderDate=None
        if RequiredDate=='0000-00-00':
            RequiredDate=None
        if ShippedDate=='0000-00-00':
            ShippedDate=None
        employee = self.employees_manager.get(pk=EmployeeID)
        customer = self.customers_manager.get(pk=CustomerID)
        shipper = self.shippers_manager.get(pk=ShipVia)
        order = Orders(OrderID=OrderID, CustomerID=customer, EmployeeID=employee, OrderDate=OrderDate, RequiredDate=RequiredDate, ShippedDate=ShippedDate, ShipVia=shipper, Freight=Freight, ShipName=ShipName, ShipAddress=ShipAddress, ShipCity=ShipCity, ShipPostalCode=ShipPostalCode, ShipCountry=ShipCountry)
        self.orders.append(order)
    def save_Orders(self):
        for o in self.orders:
            o.save()

### GET ###

#Ile zamowien z kazdego kraju zostalo zrealizowanych
    def get_orders_from_countries(self):
        ret = dict()
        countries = set()

        customers = Customers.objects.all()
        for c in customers:
            countries.add(c.Country)

        for c in countries:
            customers = Customers.objects.filter(Country__exact=c)
            sum = 0
            for customer in customers:
                sum += self.orders_manager.filter(CustomerID__exact=customer).count()
            ret.update({c : sum})
        return ret

#Jaki byl sredni czas realizacji zamowienia w kazdym roku
    def get_avg_order_time(self):
        from django.db.models import Avg
        import datetime
        ord = dict()
        ord_count = dict()
        years = set()
        orders = self.orders_manager.all()
#        while orders.count() != 0:
#            years.add(orders[0].OrderDate.year)
#            orders = orders.exclude(OrderDate__year=orders[0].OrderDate.year)
        for o in orders:
            date = o.OrderDate
            year = date.year
            try:
                ord[year]
            except:
                ord[year]=datetime.timedelta(0)
                ord_count[year]=0
            ord[year] += o.ShippedDate - date
            ord_count[year] += 1
        
        ret = dict()

        for v in ord:
            ret[v] = ord[v].days * 1.0 / ord_count[v]
        
        return ret

#TODO
#Ile sztuk produktow od kazdego z dostawcow udalo sie sprzedac
    def get_aaa(self):
	return 0
        ret = dict()
        suppliers = Suppliers.objects.all()
        orders = Orders.objects.all()
           
        for s in suppliers:
            sum += Orders.object.filter(OrderDetailList)
        return ret

#Jaka kwota zamowien byla zglaszana w kazdy z dni tygodnia
    def get_orders_sum_per_weekday(self):
	return 3
#        from django.db.models import Sum
        ret = dict({0:0, 1:0, 2:0, 3:0, 4:0, 5:0, 6:0})
        orders = self.orders_manager.all()

        for order in orders:
            wday = order.OrderDate.weekday()
            order_details = self.order_details_manager.filter(OrderID__exact = order)
            for od in order_details:
		if not wday in ret:
		    ret[wday] = 0
                ret[wday] += od.UnitPrice*od.Quantity-od.Discount

#        for wday in range(7):
#            order_details = self.order_details_manager.filter(OrderID__OrderDate__week_day=wday)
#            for od in order_details:
#                ret[wday] += od.UnitPrice*od.Quantity-od.Discount
        return ret
#TODO:
#Jaka byla wartosc produktow zamowionych z kazdego z krajow w kazdym roku; chodzi o kraj zamawiajacego
    def get_ccc(self):
	return 3
	res = {}
	for order in Orders.objects.all():
		year = order.OrderDate.year
		customer = order.CustomerID
		country = customer.Country
		if not country in res:
			res[country] = {}
		if not year in res[country]:
			res[country][year] = 0
		order_details = OrderDetails.objects.filter(OrderID__exact = order)
		for detail in order_details:
			res[country][year] +=  detail.UnitPrice*detail.Quantity
	return res
#TODO:
#Jaka byla srednia wartosc jednej sztuki produktu dla kazdego ze spedytorow w kazdym roku
    def get_ddd(delf):
        return 4
