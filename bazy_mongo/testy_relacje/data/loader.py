#!/usr/bin/python

import xml.etree.ElementTree as ET
import os
import yaml
import pickle

northwind_database = {}

files = os.listdir(".")
#print files

for fil in files:
	if fil.endswith(".xml"):
		if fil=="orders.xml" or fil=="orderdetails.xml":
			continue
		print "Processing file " + fil + "..."
		tree = ET.parse(fil)
		root = tree.getroot()
		if root.tag not in northwind_database:
			
			northwind_database[root.tag] = []
		for child in root:
			row = {}
			for olderchild in child:
				row[olderchild.tag] = olderchild.text
			northwind_database[root.tag].append(row)

#for detail in northwind_database["Orders"]:
#	print detail["OrderID"]

#os.exit(0)


stream = file("northwind.pickle", "w")
pickle.dump(northwind_database, stream)

print "Pickle created. Finished"
