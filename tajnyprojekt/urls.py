from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
#from django.contrib import admin
#admin.autodiscover()

from django.views.generic import ListView, DetailView
from tajnaaplikacja.models import Post


urlpatterns = patterns('',
    # Examples:
#    url(r'^$', 'tajnyprojekt.views.home', name='home'),
#    url(r'^tajnyprojekt/', include('tajnyprojekt.foo.urls')),


    url(r'^$', ListView.as_view(queryset=Post.objects.all(), context_object_name="posts_list"), name="home"),

    url(r'^post/(?P<slug>[a-zA-Z0-9-]+)/$', DetailView.as_view( queryset=Post.objects.all(), context_object_name="post"), name="post"),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
