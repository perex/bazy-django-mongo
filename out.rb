table "Alphabetical list of products", :ignore => true do
	column "ProductID", :integer
	column "ProductName", :string
	column "SupplierID", :integer
	column "CategoryID", :integer
	column "QuantityPerUnit", :string
	column "UnitPrice", :decimal
	column "UnitsInStock", :integer
	column "UnitsOnOrder", :integer
	column "ReorderLevel", :integer
	column "Discontinued", :binary
	column "CategoryName", :string
end

table "Categories", :embed_in => :Products, :on => :CategoryID do
	column "CategoryID", :integer
	column "CategoryName", :string
	column "Description", :text
	column "Picture", :binary, :ignore => true
end

table "Category Sales for 1997", :ignore => true do
	column "CategoryName", :string
	column "CategorySales", :float
end

table "Current Product List", :ignore => true do
	column "ProductID", :integer
	column "ProductName", :string
end

table "Customer and Suppliers by City", :ignore => true do
	column "City", :string
	column "CompanyName", :string
	column "ContactName", :string
	column "Relationship", :string
end

table "CustomerCustomerDemo", :embed_in => 'Customers', :on => 'CustomerID' do
	column "CustomerID", :string, :ignore => true
	column "CustomerTypeID", :string, :ignore => true
end

table "CustomerDemographics", :embed_in => 'CustomerCustomerDemo', :on => 'CustomerTypeID' do
	column "CustomerTypeID", :string
	column "CustomerDesc", :text
end

table "Customers" do
	column "CustomerID", :string
	column "CompanyName", :string
	column "ContactName", :string
	column "ContactTitle", :string
	column "Address", :string
	column "City", :string
	column "Region", :string
	column "PostalCode", :string
	column "Country", :string
	column "Phone", :string
	column "Fax", :string
end

table "EmployeeTerritories" do
	column "EmployeeID", :integer
	column "TerritoryID", :string
end

table "Employees" do
	column "EmployeeID", :integer
	column "LastName", :string
	column "FirstName", :string
	column "Title", :string
	column "TitleOfCourtesy", :string
	column "BirthDate", :datetime
	column "HireDate", :datetime
	column "Address", :string
	column "City", :string
	column "Region", :string
	column "PostalCode", :string
	column "Country", :string
	column "HomePhone", :string
	column "Extension", :string
	column "Photo", :binary, :ignore => true
	column "Notes", :text
	column "ReportsTo", :integer
	column "PhotoPath", :string
	column "Salary", :float
end

table "Invoices", :ignore => true do
	column "ShipName", :string
	column "ShipAddress", :string
	column "ShipCity", :string
	column "ShipRegion", :string
	column "ShipPostalCode", :string
	column "ShipCountry", :string
	column "CustomerID", :string
	column "CustomerName", :string
	column "Address", :string
	column "City", :string
	column "Region", :string
	column "PostalCode", :string
	column "Country", :string
	column "Salesperson", :float
	column "OrderID", :integer
	column "OrderDate", :datetime
	column "RequiredDate", :datetime
	column "ShippedDate", :datetime
	column "ShipperName", :string
	column "ProductID", :integer
	column "ProductName", :string
	column "UnitPrice", :decimal
	column "Quantity", :integer
	column "Discount", :float
	column "ExtendedPrice", :float
	column "Freight", :decimal
end

table "OrderDetails" do
	column "OrderID", :integer
	column "ProductID", :integer
	column "UnitPrice", :decimal
	column "Quantity", :integer
	column "Discount", :float
end

table "Order Details Extended", :ignore => true  do
	column "OrderID", :integer
	column "ProductID", :integer
	column "ProductName", :string
	column "UnitPrice", :decimal
	column "Quantity", :integer
	column "Discount", :float
	column "ExtendedPrice", :float
end

table "Order Subtotals", :ignore => true do
	column "OrderID", :integer
	column "Subtotal", :float
end

table "Orders" do
	column "OrderID", :integer
	column "CustomerID", :string
	column "EmployeeID", :integer
	column "OrderDate", :datetime
	column "RequiredDate", :datetime
	column "ShippedDate", :datetime
	column "ShipVia", :integer
	column "Freight", :decimal
	column "ShipName", :string
	column "ShipAddress", :string
	column "ShipCity", :string
	column "ShipRegion", :string
	column "ShipPostalCode", :string
	column "ShipCountry", :string
end

table "Orders Qry", :ignore => true do
	column "OrderID", :integer
	column "CustomerID", :string
	column "EmployeeID", :integer
	column "OrderDate", :datetime
	column "RequiredDate", :datetime
	column "ShippedDate", :datetime
	column "ShipVia", :integer
	column "Freight", :decimal
	column "ShipName", :string
	column "ShipAddress", :string
	column "ShipCity", :string
	column "ShipRegion", :string
	column "ShipPostalCode", :string
	column "ShipCountry", :string
	column "CompanyName", :string
	column "Address", :string
	column "City", :string
	column "Region", :string
	column "PostalCode", :string
	column "Country", :string
end

table "Product Sales for 1997", :ignore => true do
	column "CategoryName", :string
	column "ProductName", :string
	column "ProductSales", :float
end

table "Products" do
	column "ProductID", :integer
	column "ProductName", :string
	column "SupplierID", :integer
	column "CategoryID", :integer, :references => :Categories, :on => :CategoryID 
	column "QuantityPerUnit", :string
	column "UnitPrice", :decimal
	column "UnitsInStock", :integer
	column "UnitsOnOrder", :integer
	column "ReorderLevel", :integer
	column "Discontinued", :binary
end

table "Products Above Average Price", :ignore => true do
	column "ProductName", :string
	column "UnitPrice", :decimal
end

table "Products by Category", :ignore => true do
	column "CategoryName", :string
	column "ProductName", :string
	column "QuantityPerUnit", :string
	column "UnitsInStock", :integer
	column "Discontinued", :binary
end

table "Quarterly Orders", :ignore => true do
	column "CustomerID", :string
	column "CompanyName", :string
	column "City", :string
	column "Country", :string
end

table "Region" do
	column "RegionID", :integer
	column "RegionDescription", :string
end

table "Sales Totals by Amount", :ignore => true do
	column "SaleAmount", :float
	column "OrderID", :integer
	column "CompanyName", :string
	column "ShippedDate", :datetime
end

table "Sales by Category", :ignore => true do
	column "CategoryID", :integer
	column "CategoryName", :string
	column "ProductName", :string
	column "ProductSales", :float
end

table "Shippers" do
	column "ShipperID", :integer
	column "CompanyName", :string
	column "Phone", :string
end

table "Summary of Sales by Quarter", :ignore => true do
	column "ShippedDate", :datetime
	column "OrderID", :integer
	column "Subtotal", :float
end

table "Summary of Sales by Year", :ignore => true do
	column "ShippedDate", :datetime
	column "OrderID", :integer
	column "Subtotal", :float
end

table "Suppliers" do
	column "SupplierID", :integer
	column "CompanyName", :string
	column "ContactName", :string
	column "ContactTitle", :string
	column "Address", :string
	column "City", :string
	column "Region", :string
	column "PostalCode", :string
	column "Country", :string
	column "Phone", :string
	column "Fax", :string
	column "HomePage", :text
end

table "Territories" do
	column "TerritoryID", :string
	column "TerritoryDescription", :string
	column "RegionID", :integer
end

